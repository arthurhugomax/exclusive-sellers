<?php

class UpdateSellerAddressTest extends TestCase
{    
    /**
     * test Update Seller PF
     *
     * @return void
     */
    public function testUpdateSellerAddress()
    {
        $this->withoutMiddleware();
        //
        $data = [
            'zipCode' => '13212-052',
            'street' => 'Rua Victorio Dinazio',
            'number' => '317',
            'neighborhood' => 'Jardim Tannus',
            'city' => 'Jundiaí',
            'state' => 'SP'
        ];
        //
        $this->put('/api/sellers/1/address/1', $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'sellerAddress' => [
                'id',
                'zipCode',
                'street',
                'number',
                'neighborhood',
                'city',
                'state',
                'seller_id',
                'updated_at',
                'created_at'
            ]
        ]);
    }
    
}
