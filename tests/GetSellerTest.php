<?php

use Faker\Factory;

class GetSellerTest extends TestCase
{    
    /**
     * test Get Seller
     *
     * @return void
     */
    public function testGetSeller()
    {
        $this->withoutMiddleware();

        $this->get('/api/sellers/1');
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'seller' => [
                'id',
                'name',
                'phone',
                'type',
                'document',
                'status',
                'updated_at',
                'created_at'
            ]
        ]);
    }

}
