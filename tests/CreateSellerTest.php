<?php

use Faker\Factory;

class CreateSellerTest extends TestCase
{    
    /**
     * test create seller
     *
     * @return void
     */
    public function testCreateSeller()
    {
        $this->withoutMiddleware();
        
        $faker = Factory::create('pt_BR');

        // PF
        $data = [
            'name' => $faker->name(),
            'email' => $faker->email(),
            'phone' => $faker->phone(),
            'type' => 'PF',
            'document' => $faker->cpf()
        ];

        $this->post('/api/sellers', $data);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'seller' => [
                'id',
                'name',
                'phone',
                'type',
                'document',
                'status',
                'updated_at',
                'created_at'
            ]
        ]);

        // PJ
        $data['email'] = $faker->email();
        $data['type'] = 'PJ';
        $data['document'] = $faker->cnpj();

        $this->post('/api/sellers', $data);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'seller' => [
                'id',
                'name',
                'phone',
                'type',
                'document',
                'status',
                'updated_at',
                'created_at'
            ]
        ]);
    }

}
