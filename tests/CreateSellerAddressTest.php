<?php

use Faker\Factory;

class CreateSellerAddressTest extends TestCase
{    
    /**
     * test Create Seller PF
     *
     * @return void
     */
    public function testCreateSellerAddress()
    {
        $this->withoutMiddleware();
        
        $faker = Factory::create('pt_BR');
        
        // create seller
        $data = [
            'name' => $faker->name(),
            'email' => $faker->email(),
            'phone' => $faker->phone(),
            'type' => 'PF',
            'document' => $faker->cpf()
        ];

        $this->post('/api/sellers', $data);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'seller' => [
                'id',
                'name',
                'phone',
                'type',
                'document',
                'status',
                'updated_at',
                'created_at'
            ]
        ]);
        //
        $data = [
            'zipCode' => '13212-052',
            'street' => 'Rua Victorio Dinazio',
            'number' => '317',
            'neighborhood' => 'Jardim Tannus',
            'city' => 'Jundiaí',
            'state' => 'SP'
        ];
        //
        $this->post('/api/sellers/1/address', $data);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'sellerAddress' => [
                'id',
                'zipCode',
                'street',
                'number',
                'neighborhood',
                'city',
                'state',
                'seller_id',
                'updated_at',
                'created_at'
            ]
        ]);
    }
    
}
