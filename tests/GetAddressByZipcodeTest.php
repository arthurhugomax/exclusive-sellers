<?php

use Faker\Factory;

class GetAddressByZipCodeTest extends TestCase
{    
        
    /**
     * testGetAddressByZipCode
     *
     * @return void
     */
    public function testGetAddressByZipCode()
    {
        $this->withoutMiddleware();

        $data = [
            'zipCode' => '13212-052'
        ];

        $this->post('/api/address', $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'address' => [
                'zipCode',
                'street',
                'complement',
                'neighborhood',
                'state',
                'ibge'
            ]
        ]);
    }

}
