<?php

use Faker\Factory;

class AuthTest extends TestCase
{
        
    /**
     * test Auth
     *
     * @return void
     */
    public function testAuth()
    {
        $faker = Factory::create('pt_BR');
        $name = $faker->name();
        $email = $faker->email();
        $pass = strval(rand());
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $pass
        ];
        // register
        $this->post('/api/register', $data);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'user' => [
                'id',
                'name',
                'email',
                'updated_at',
                'created_at'
            ],
            'message'
        ]);
        // auth
        unset($data['name']);
        $this->post('/api/auth', $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'token',
            'token_type',
            'expires_in'
        ]);
    }
}
