<?php

use Faker\Factory;

class GetSellersTest extends TestCase
{    
    /**
     * test Get Seller
     *
     * @return void
     */
    public function testGetSellers()
    {
        $this->withoutMiddleware();

        $this->get('/api/sellers');
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'sellers' => [
                '*' => [
                    'id',
                    'name',
                    'phone',
                    'type',
                    'document',
                    'status',
                    'updated_at',
                    'created_at'
                ]
            ]
        ]);
    }

}
