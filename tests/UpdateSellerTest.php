<?php

use Faker\Factory;

class UpdateSellerTest extends TestCase
{    
    /**
     * test Update Seller PF
     *
     * @return void
     */
    public function testUpdateSeller()
    {
        $this->withoutMiddleware();
        
        $faker = Factory::create('pt_BR');

        $data = [
            'name' => $faker->name(),
            'email' => $faker->email(),
            'phone' => $faker->phone(),
            'type' => 'PF',
            'document' => $faker->cpf()
        ];

        $this->put('/api/sellers/1', $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'seller' => [
                'id',
                'name',
                'phone',
                'type',
                'document',
                'status',
                'updated_at',
                'created_at'
            ]
        ]);

    }

}
