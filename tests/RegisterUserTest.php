<?php

use Faker\Factory;
class RegisterUserTest extends TestCase
{
        
    /**
     * test Register User
     *
     * @return void
     */
    public function testRegisterUser()
    {
        
        $faker = Factory::create('pt_BR');
        $data = [
            'name' => $faker->name(),
            'email' => $faker->email(),
            'password' => strVal(rand())
        ];

        $this->post('/api/register', $data);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'user' => [
                'id',
                'name',
                'email',
                'updated_at',
                'created_at'
            ],
            'message'
        ]);
    }
}
