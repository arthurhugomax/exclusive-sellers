<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use App\Models\SellerAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\LogController;

class SellerController extends Controller
{    
    /**
     * Instantiate instance
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Get all Sellers
     *
     * @return mixed
     */
    public function getAllSellers()
    {
        return response()->json(['sellers' => Seller::all()], 200);
    }
    
    /**
     * Get Seller by id
     *
     * @param  mixed $id
     * @return mixed
     */
    public function getSeller($id)
    {
        try {
            $seller = Seller::findOrFail($id);
            $sellerAddress = SellerAddress::where('seller_id', '=', $id)->get();
            if (!empty($sellerAddress)) {
                $seller['address'] = $sellerAddress;
            }
            return response()->json(['seller' => $seller], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Seller not found!'], 404);
        }
    }
    
    /**
     * Create Seller
     *
     * @param  mixed $request
     * @return mixed
     */
    public function createSeller(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|email|unique:sellers|max:55',
            'phone' => 'required|string|max:15',
            'type' => 'required|string|max:2',
            'status' => 'string|max:1',
            'document' => 'required|string|max:20'
        ]);
        //
        $log = new LogController();
        try {
            $data = $this->formatSellerRequest($request);
            if (!$data) {
                return response()->json(['message' => 'Failed to format seller data!'], 409);
            }
            // se não vier com o status na requisição, salvar como Ativo
            if (!isset($data['status']) || empty($data['status'])) {
                $data['status'] = 'A';
            }
            $seller = Seller::create($data);
            $response = ['seller' => $seller];
            //saving log
            $log->createLog('CREATE SELLER', json_encode($request->all()), json_encode($response));
            //
            return response()->json($response, 201);
        } catch (\Exception $e) {
            $log->createLog('CREATE SELLER ERROR', json_encode($request->all()), json_encode(['message' => $e->getMessage()]));
            return response()->json(['message' => 'Create seller failed!'], 409);
        }
    }
    
    /**
     * Update Seller
     *
     * @param  mixed $id
     * @param  mixed $request
     * @return mixed
     */
    public function updateSeller($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'string|max:100',
            'email' => 'email|unique:sellers|max:55',
            'phone' => 'string|max:15',
            'type' => 'string|max:2',
            'status' => 'string|max:1',
            'document' => 'string|max:15'
        ]);
        //
        $log = new LogController();
        try {
            $seller = Seller::findOrFail($id);
            $seller->update($request->all());
            $response = ['seller' => $seller];
            //saving log
            $log->createLog('UPDATE SELLER', json_encode($request->all()), json_encode($response));
            //
            return response()->json($response, 200);
        } catch (\Exception $e) {
            $log->createLog('UPDATE SELLER ERROR', json_encode($request->all()), json_encode(['message' => $e->getMessage()]));
            return response()->json(['message' => 'Update seller failed!'], 409);
        }
    }
    
    /**
     * Delete Seller
     *
     * @param  mixed $id
     * @return mixed
     */
    public function deleteSeller($id)
    {
        try {
            $log = new LogController();
            $seller = Seller::findOrFail($id)->delete();
            $response = ['seller' => $seller];
            //saving log
            $log->createLog('DELETE SELLER', json_encode($id), json_encode($response));
            //
            return response()->json(['message' => 'Deleted successfully'], 200);
        } catch (\Exception $e) {
            $log->createLog('DELETE SELLER ERROR', json_encode($id), json_encode(['message' => $e->getMessage()]));
            return response()->json(['message' => 'Seller not found!'], 404);
        }
        
    }
    
    
    /**
     * formatSellerRequest
     *
     * @param  mixed $request
     * @return mixed
     */
    private function formatSellerRequest(Request $request)
    {
        try {
            $fieldsFormatted = ['phone', 'document'];
            $data = $request->all();
            $keys = array_keys($data);
            foreach ($keys as $key) {
                if (in_array($key, $fieldsFormatted)) {
                    $value = preg_replace('/[^0-9]/', '', (string) $data[$key]);
                    $data[$key] = $value;
                }
            }
            return $data;
        } catch (\Exception $e) {
            return false;
        }
    }
}