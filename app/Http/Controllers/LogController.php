<?php

namespace App\Http\Controllers;

use App\Models\Log;

class LogController extends Controller
{    
    /**
     * Instantiate a new instance
     */
    public function __construct()
    {

    }
    
    /**
     * createLog
     *
     * @param  mixed $type
     * @param  mixed $request
     * @param  mixed $response
     * @return boolean
     */
    public function createLog($type, $request, $response)
    {
        try {
            $data = [
                'type' => $type,
                'request' => $request,
                'response' => $response
            ];
            Log::create($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    
}