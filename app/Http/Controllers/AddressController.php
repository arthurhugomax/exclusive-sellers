<?php

namespace App\Http\Controllers;

use App\Models\ViaCEP;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Instantiate instance
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


        
    /**
     * get Address by zipcode
     *
     * @param  mixed $request
     * @return void
     */
    public function getAddress(Request $request)
    {
        try {
            $data = $request->all();
            $zipCode = preg_replace('/[^0-9]/', '', (string) $data['zipCode']);
            $viacep = new ViaCEP;
            $address = $viacep->findByZipCode($zipCode)->toArray();

            if (!empty($address)) {
                return response()->json(['address' => $address], 200);
            }
            throw new \Exception('Address not found!');
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }

}