<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use App\Models\SellerAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\LogController;

class SellerAddressController extends Controller
{
    /**
     * Instantiate instance
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get address by seller id
     *
     * @param  mixed $id
     * @return mixed
     */
    public function getSellerAddress($sellerId)
    {
        try {
            // first find seller to validate
            $seller = Seller::findOrFail($sellerId);
            // find seller address
            $sellerAddress = SellerAddress::where('seller_id', '=', $sellerId)->get();
            return response()->json(['sellerAddress' => $sellerAddress], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Seller address not found!'], 404);
        }
    }

    /**
     * Create seller address
     *
     * @param  mixed $request
     * @return mixed
     */
    public function createSellerAddress($sellerId, Request $request)
    {
       
        $this->validate($request, [
            'zipCode' => 'required|string',
            'street' => 'required|string',
            'number' => 'required|string',
            'neighborhood' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string'
        ]);
        // first find seller to validate
        $seller = Seller::findOrFail($sellerId);
        //
        $log = new LogController();
        try {
            $data = $this->formatSellerAddressRequest($request);
            if (!$data) {
                return response()->json(['message' => 'Failed to format seller address data!'], 409);
            }
            $data['seller_id'] = $sellerId;
            $sellerAddress = SellerAddress::create($data);
            $response = ['sellerAddress' => $sellerAddress];
            //saving log
            $log->createLog('CREATE SELLER ADDRESS', json_encode($request->all()), json_encode($response));
            //
            return response()->json($response, 201);
        } catch (\Exception $e) {
            $log->createLog('CREATE SELLER ADDRESS ERROR', json_encode($request->all()), json_encode(['message' => $e->getMessage()]));
            return response()->json(['message' => 'Create seller address failed!'], 409);
        }
    }

    /**
     * Update seller address
     *
     * @param  mixed $id
     * @param  mixed $request
     * @return mixed
     */
    public function updateSellerAddress($sellerId, $addressId, Request $request)
    {
        $this->validate($request, [
            'zipCode' => 'required|string',
            'street' => 'required|string',
            'number' => 'required|string',
            'neighborhood' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string'
        ]);
        // first find seller to validate
        $seller = Seller::findOrFail($sellerId);
        //
        $log = new LogController();
        try {
            $data = $this->formatSellerAddressRequest($request);
            if (!$data) {
                return response()->json(['message' => 'Failed to format seller address data!'], 409);
            }
            $sellerAddress = SellerAddress::findOrFail($addressId);
            $sellerAddress->update($data);
            $response = ['sellerAddress' => $sellerAddress];
            //saving log
            $log->createLog('UPDATE SELLER ADDRESS', json_encode($request->all()), json_encode($response));
            //
            return response()->json($response, 200);
        } catch (\Exception $e) {
            $log->createLog('UPDATE SELLER ADDRESS ERROR', json_encode($request->all()), json_encode(['message' => $e->getMessage()]));
            return response()->json(['message' => 'Update seller address failed!'], 409);
        }
    }

    /**
     * Delete seller
     *
     * @param  mixed $id
     * @return mixed
     */
    public function deleteSellerAddress($sellerId, $addressId)
    {
        try {
            // first find seller to validate
            $seller = Seller::findOrFail($sellerId);
            //
            $log = new LogController();
            $sellerAddress = SellerAddress::findOrFail($addressId)->delete();
            $response = ['sellerAddress' => $sellerAddress];
            //saving log
            $log->createLog('DELETE SELLER ADDRESS', json_encode($addressId), json_encode($response));
            //
            return response()->json(['message' => 'Deleted successfully'], 200);
        } catch (\Exception $e) {
            $log->createLog('DELETE SELLER ADDRESS ERROR', json_encode($addressId), json_encode(['message' => $e->getMessage()]));
            return response()->json(['message' => 'Seller address not found!'], 404);
        }

    }

    /**
     * Format seller request
     *
     * @param  mixed $request
     * @return mixed
     */
    private function formatSellerAddressRequest(Request $request)
    {
        try {
            $fieldsFormatted = ['zipCode', 'number'];
            $data = $request->all();
            $keys = array_keys($data);
            foreach ($keys as $key) {
                if (in_array($key, $fieldsFormatted)) {
                    $value = preg_replace('/[^0-9]/', '', (string) $data[$key]);
                    $data[$key] = $value;
                }
            }
            return $data;
        } catch (\Exception $e) {
            return false;
        }
    }
}