<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Store a new user
     *
     * @param  Request  $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|email|unique:users|max:55',
            'password' => 'required|min:5',
        ]);
        try {
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $password = $request->input('password');
            $user->password = app('hash')->make($password);
            $user->save();
            return response()->json(['user' => $user, 'message' => 'created'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }

    }
    
    /**
     * Auth on service
     *
     * @param  mixed $request
     * @return mixed
     */
    public function auth(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        try {
            $credentials = $request->only(['email', 'password']);
            $token = Auth::attempt($credentials);
            if (!$token) {
                return response()->json(['message' => 'unauthorized'], 401);
            }
            return $this->respondWithToken($token);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }
}