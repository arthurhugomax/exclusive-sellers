<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SellerAddress extends Model
{
    
    /**
     * table name
     *
     * @var string
     */
    protected $table = "seller_address";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seller_id', 'zipCode', 'street', 'number', 'neighborhood', 'city', 'state',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
