<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'request', 'response',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * create Log
     *
     * @param  mixed $type
     * @param  mixed $request
     * @param  mixed $response
     * @return boolean
     */
    public function createLog($type, $request, $response)
    {
        try {
            $data = [
                'type' => $type,
                'request' => $request,
                'response' => $response
            ];
            Log::create($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
