<?php

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

class ViaCEP
{
    /**
     * HTTP client
     *
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * Create a new ZipCode class instance
     *
     * @param \GuzzleHttp\ClientInterface $http
     */
    public function __construct(ClientInterface $http = null)
    {
        $this->http = $http ?: new Client;
    }
 
    /**
     * findByZipCode
     *
     * @param  mixed $zipCode
     * @return mixed
     */
    public function findByZipCode($zipCode)
    {
        $url = sprintf('https://viacep.com.br/ws/%s/json', $zipCode);

        $response = $this->http->request('GET', $url);

        $attributes = json_decode($response->getBody(), true);

        if (array_key_exists('erro', $attributes) && $attributes['erro'] === true) {
            return new Address;
        }

        return new Address($attributes);
    }
}