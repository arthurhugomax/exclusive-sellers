<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Exclusive Sellers Service 1.0';
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/', function () use ($router) {
        return 'Exclusive Sellers Service 1.0';
    });
    // users
    $router->post('register', ['uses' => 'AuthController@register']);
    // auth
    $router->post('auth', ['uses' => 'AuthController@auth']);
    // sellers
    $router->get('sellers', ['uses' => 'SellerController@getAllSellers']);
    $router->get('sellers/{id}', ['uses' => 'SellerController@getSeller']);
    $router->post('sellers', ['uses' => 'SellerController@createSeller']);
    $router->delete('sellers/{id}', ['uses' => 'SellerController@deleteSeller']);
    $router->put('sellers/{id}', ['uses' => 'SellerController@updateSeller']);
    // seller address
    $router->get('sellers/{sellerId}/address', ['uses' => 'SellerAddressController@getSellerAddress']);
    $router->post('sellers/{sellerId}/address', ['uses' => 'SellerAddressController@createSellerAddress']);
    $router->put('sellers/{sellerId}/address/{addressId}', ['uses' => 'SellerAddressController@updateSellerAddress']);
    $router->delete('sellers/{sellerId}/address/{addressId}', ['uses' => 'SellerAddressController@deleteSellerAddress']);
    // get address by zipCode (using viacep.com.br)
    $router->post('address', ['uses' => 'AddressController@getAddress']);
});