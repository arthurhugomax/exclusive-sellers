-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 06/03/2023 às 23:39
-- Versão do servidor: 10.4.27-MariaDB
-- Versão do PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `exclusive-sellers`
--
CREATE DATABASE IF NOT EXISTS `exclusive-sellers` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `exclusive-sellers`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `logs`
--

CREATE TABLE `logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `request` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`request`)),
  `response` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`response`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `logs`
--

INSERT INTO `logs` (`id`, `type`, `request`, `response`, `created_at`, `updated_at`) VALUES
(1, 'CREATE SELLER', '{\"name\":\"Jo\\u00e3o Fl\\u00e1vio da Silva\",\"email\":\"joaoflavio@gmail.com\",\"phone\":\"(11) 96476-1378\",\"type\":\"PJ\",\"document\":\"23.663.478\\/0001-24\"}', '{\"seller\":{\"name\":\"Jo\\u00e3o Fl\\u00e1vio da Silva\",\"email\":\"joaoflavio@gmail.com\",\"phone\":\"11964761378\",\"type\":\"PJ\",\"document\":\"23663478000124\",\"status\":\"A\",\"updated_at\":\"2023-03-06T22:04:50.000000Z\",\"created_at\":\"2023-03-06T22:04:50.000000Z\",\"id\":1}}', '2023-03-06 22:04:50', '2023-03-06 22:04:50'),
(2, 'CREATE SELLER ADDRESS', '{\"zipCode\":\"13212-052\",\"street\":\"Rua Victorio Dinazio\",\"number\":\"317\",\"neighborhood\":\"Jardim Tannus\",\"city\":\"Jundia\\u00ed\",\"state\":\"SP\"}', '{\"sellerAddress\":{\"zipCode\":\"13212052\",\"street\":\"Rua Victorio Dinazio\",\"number\":\"317\",\"neighborhood\":\"Jardim Tannus\",\"city\":\"Jundia\\u00ed\",\"state\":\"SP\",\"seller_id\":\"1\",\"updated_at\":\"2023-03-06T22:06:25.000000Z\",\"created_at\":\"2023-03-06T22:06:25.000000Z\",\"id\":1}}', '2023-03-06 22:06:25', '2023-03-06 22:06:25'),
(3, 'UPDATE SELLER', '{\"name\":\"Jo\\u00e3o de Deus\"}', '{\"seller\":{\"id\":1,\"name\":\"Jo\\u00e3o de Deus\",\"email\":\"joaoflavio@gmail.com\",\"status\":\"A\",\"phone\":\"11964761378\",\"type\":\"PJ\",\"document\":\"23663478000124\",\"created_at\":\"2023-03-06T22:04:50.000000Z\",\"updated_at\":\"2023-03-06T22:06:46.000000Z\"}}', '2023-03-06 22:06:46', '2023-03-06 22:06:46'),
(4, 'UPDATE SELLER', '{\"name\":\"Jo\\u00e3o de Deus\",\"phone\":\"11964761379\"}', '{\"seller\":{\"id\":1,\"name\":\"Jo\\u00e3o de Deus\",\"email\":\"joaoflavio@gmail.com\",\"status\":\"A\",\"phone\":\"11964761379\",\"type\":\"PJ\",\"document\":\"23663478000124\",\"created_at\":\"2023-03-06T22:04:50.000000Z\",\"updated_at\":\"2023-03-06T22:07:10.000000Z\"}}', '2023-03-06 22:07:10', '2023-03-06 22:07:10'),
(5, 'UPDATE SELLER ADDRESS', '{\"zipCode\":\"13212-052\",\"street\":\"Rua Victorio Dinazio\",\"number\":\"317\",\"neighborhood\":\"Jardim Tannus\",\"city\":\"Jundia\\u00ed\",\"state\":\"SP\"}', '{\"sellerAddress\":{\"id\":1,\"seller_id\":1,\"zipCode\":\"13212052\",\"street\":\"Rua Victorio Dinazio\",\"number\":\"317\",\"neighborhood\":\"Jardim Tannus\",\"city\":\"Jundia\\u00ed\",\"state\":\"SP\",\"created_at\":\"2023-03-06T22:06:25.000000Z\",\"updated_at\":\"2023-03-06T22:06:25.000000Z\"}}', '2023-03-06 22:07:40', '2023-03-06 22:07:40');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(19, '2023_03_04_200111_create_sellers_table', 1),
(20, '2023_03_04_201400_create_seller_address', 1),
(21, '2023_03_04_214541_create_users_table', 1),
(22, '2023_03_07_224225_create_logs_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `sellers`
--

CREATE TABLE `sellers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(55) NOT NULL,
  `status` varchar(1) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `type` varchar(2) NOT NULL,
  `document` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `sellers`
--

INSERT INTO `sellers` (`id`, `name`, `email`, `status`, `phone`, `type`, `document`, `created_at`, `updated_at`) VALUES
(1, 'João de Deus', 'joaoflavio@gmail.com', 'A', '11964761379', 'PJ', '23663478000124', '2023-03-06 22:04:50', '2023-03-06 22:07:10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `seller_address`
--

CREATE TABLE `seller_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seller_id` bigint(20) UNSIGNED NOT NULL,
  `zipCode` int(11) NOT NULL,
  `street` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `neighborhood` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `seller_address`
--

INSERT INTO `seller_address` (`id`, `seller_id`, `zipCode`, `street`, `number`, `neighborhood`, `city`, `state`, `created_at`, `updated_at`) VALUES
(1, 1, 13212052, 'Rua Victorio Dinazio', '317', 'Jardim Tannus', 'Jundiaí', 'SP', '2023-03-06 22:06:25', '2023-03-06 22:06:25');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Arthur Hugo Maximiliano', 'teste@gmail.com', '$2y$10$ui0fuIUfcqaFmqQTZ7SanuPAcBVoDu0ZIf/eZTOaMFDIZkQfy6iii', '2023-03-06 22:02:05', '2023-03-06 22:02:05');

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `seller_address`
--
ALTER TABLE `seller_address`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `seller_address`
--
ALTER TABLE `seller_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
